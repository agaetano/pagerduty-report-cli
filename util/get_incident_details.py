# import libraries
import json

# import libraries from other files
from util.progress_bar import progressBar
from util.make_https_request import make_https_inc_details


# get more details for all the incidents
def get_inc_details(pd_key, month, all_incidents_file, incident_details_file):

    print('Getting incident details')

    sorted_incidents = {
        f'{month} incidents': {
            'total count': 0,
            'incidents': {}
        }
    }

    with open(all_incidents_file) as incidents:
        unsorted_incidents = json.load(incidents)

        total = len(unsorted_incidents['incidents'])

        counter = 0

        for inc in unsorted_incidents['incidents']:
            counter = counter + 1
            progressBar(counter, total)

            inc_host = make_https_inc_details(pd_key, inc['id'])

            title_to_lower = inc['title'].lower() # Convert the text to lowercase to search more thoroughly

            '''
            # Group common alerts by title
            if 'system.mem' in title_to_lower or 'usable memory' in title_to_lower:
                inc['title'] = 'Memory Alerts'
            '''

            if inc['title'] not in sorted_incidents[f'{month} incidents']['incidents']:
                alert_record = {
                    inc['title']: {
                        'total count': 0,
                        'percent of total': 0,
                        'hosts': {
                            inc_host: {
                                'count': 0,
                                'percent of alert': 0,
                                'percent of total': 0
                            }
                        }
                    }
                }
                sorted_incidents[f'{month} incidents']['incidents'].update(alert_record)

            if inc_host in sorted_incidents[f'{month} incidents']['incidents'][inc['title']]['hosts']:
                count = sorted_incidents[f'{month} incidents']['incidents'][inc['title']]['hosts'][inc_host]['count']

                record = {
                    'count': count + 1
                }
                sorted_incidents[f'{month} incidents']['incidents'][inc['title']]['hosts'][inc_host].update(record)
            else:
                record = {
                    inc_host: {
                        'count': 1,
                        'percent of alert': 0,
                        'percent of total': 0
                    }
                }
                sorted_incidents[f'{month} incidents']['incidents'][inc['title']]['hosts'].update(record)

    with open(incident_details_file, 'w') as outfile:
        json.dump(sorted_incidents, outfile, indent=4)


# update the final report with the details
def update_all_incidents_details(month, incident_details_file, final_report_file):

    print('\nFinalizing report...')

    with open(incident_details_file) as incidents:
        sorted_incidents = json.load(incidents)

        # update the total count within each alert
        for alert, details in sorted_incidents[f'{month} incidents']['incidents'].items():
            total_alert_count = 0
            for host, host_details in details['hosts'].items():
                total_alert_count = total_alert_count + host_details['count']
            details.update({'total count': total_alert_count})

        # update total count for all incidents
        total_alert_count = 0
        for alert, details in sorted_incidents[f'{month} incidents']['incidents'].items():
            sorted_incidents[f'{month} incidents']['total count'] += details['total count']

        # update percent of total for each alert
        for alert, details in sorted_incidents[f'{month} incidents']['incidents'].items():
            percent = round(((details['total count'] / sorted_incidents[f'{month} incidents']['total count']) * 100), 2)
            details.update({ 'percent of total': percent } )

        # update percent of alert for each host within each alert
        for alert, details in sorted_incidents[f'{month} incidents']['incidents'].items():
            for host, host_details in details['hosts'].items():
                of_alert_percent = round(((host_details['count'] / details['total count']) * 100), 2)
                of_total_percent = round(((host_details['count'] / sorted_incidents[f'{month} incidents']['total count']) * 100), 2)
                host_details.update( { 'percent of alert': of_alert_percent, 'percent of total': of_total_percent } )

        # sort hosts by count per alert
        for alert, details in sorted_incidents[f'{month} incidents']['incidents'].items():
            new_order = {
                'hosts': {}
            }
            sorted_order = sorted(details['hosts'], key=lambda x: details['hosts'][x]['count'], reverse=True)

            for host in sorted_order:
                new_order['hosts'].update(
                    {
                        host: details['hosts'][host]
                    }
                )
            details.update(new_order)

        # sort alerts by count
        new_order = {
            'incidents': {}
        }
        sorted_order = sorted(sorted_incidents[f'{month} incidents']['incidents'], key=lambda x: sorted_incidents[f'{month} incidents']['incidents'][x]['total count'], reverse=True)

        for alert in sorted_order:
            new_order['incidents'].update(
                {
                    alert: sorted_incidents[f'{month} incidents']['incidents'][alert]
                }
            )
        sorted_incidents[f'{month} incidents'].update(new_order)

    with open(final_report_file, 'w') as outfile:
        json.dump(sorted_incidents, outfile, indent=4)