import json
import requests


# request to get basic details and count of incidents
def make_https_request(pd_key, start_date, end_date, priority, offset=0):

    team_id = 'P1NQNAT'

    url = f"https://api.pagerduty.com/incidents?timezone=EST&since={start_date}&until={end_date}&team_ids[]={team_id}&limit=100&urgencies[]={priority}&offset={offset}"

    payload = {}
    headers = {
        'Accept': 'application/vnd.pagerduty+json;version=2',
        'Authorization': f'{pd_key}'
    }

    response = requests.request("GET", url, headers=headers, data = payload)

    response = json.loads(response.text.encode('utf8'))
    return response


# request to get more details of each incident individually
def make_https_inc_details(pd_key, inc_id):

    url = f"https://api.pagerduty.com/incidents/{inc_id}/log_entries?include[]=channels&timezone=EST"

    payload = {}
    headers = {
        'Accept': 'application/vnd.pagerduty+json;version=2',
        'Authorization': pd_key
    }

    response = requests.request("GET", url, headers=headers, data = payload)

    response = json.loads(response.text.encode('utf8'))

    for record in response['log_entries']:
        if record['type'] == 'trigger_log_entry':
            if 'cef_details' in record['channel']:
                if 'source_origin' in record['channel']['cef_details']:
                    return record['channel']['cef_details']['source_origin']
                else:
                    return '-'
            else:
                return '-'
    return '-'