# import libraries
import pathlib
import shutil
import click
import json

# import libraries from other files
from util.read_parameters import read_config_parameters


# check for required files and folders and create them if necessary
def directory_setup(start_date, end_date):

    # remove the timestamp from the date
    start_date = str(start_date).split(' ')[0]
    end_date = str(end_date).split(' ')[0]

    # get the month from the dates
    month = read_config_parameters(start_date)

    # create folder paths if they do no already exist
    current_working_directory = pathlib.Path('..').parent.absolute() # sets the starting path back one directory from this files location
    cache_folder = pathlib.Path.joinpath(current_working_directory, 'cache', month)
    pathlib.Path(cache_folder).mkdir(parents = True, exist_ok = True)

    report_folder = pathlib.Path.joinpath(current_working_directory, 'reports', month)
    pathlib.Path(report_folder).mkdir(parents = True, exist_ok = True)

    secret_folder = pathlib.Path.joinpath(current_working_directory, '.secrets')
    pathlib.Path(secret_folder).mkdir(parents = True, exist_ok = True)

    # create paths for required files
    secrets_file = pathlib.Path.joinpath(secret_folder, '.pd_api_key.json')
    high_priority_file = pathlib.Path.joinpath(cache_folder, 'high_priority_incidents.json')
    low_priority_file = pathlib.Path.joinpath(cache_folder, 'low_priority_incidents.json')
    all_incidents_file = pathlib.Path.joinpath(cache_folder, 'all_incidents.json')
    incident_details_file = pathlib.Path.joinpath(cache_folder, 'all_incidents_details.json')
    final_report_file = pathlib.Path.joinpath(report_folder, f'{month}_{start_date}_{end_date}_final.json')

    create_secrets_file(secrets_file)

    # output progress of report
    print(f'Running report for: {month}')
    print(f'Report window: {start_date} - {end_date}')

    return month, cache_folder, secrets_file, high_priority_file, low_priority_file, all_incidents_file, incident_details_file, final_report_file


# cleanup folders after complete
def cleanup_folders(cache_folder):
    shutil.rmtree(cache_folder)


# create the secrets file from template
def create_secrets_file(secrets_file):

    if not secrets_file.exists():

        # create the secrets file
        pathlib.Path(secrets_file).touch(exist_ok=True)

        secrets_template = {
            "api_key": "Token token=-T4yK1KYyM2xU5Vc3j5k"
        }

        if click.confirm('Do you want to add your PagerDuty API key now?'):
            secrets_template['api_key'] = click.prompt('Enter your pagerduty API key')
        else:
            print('No API key provided. Ending execution')
            exit()

        # write the template to the file
        with open(secrets_file, 'w') as outfile:
            json.dump(secrets_template, outfile, indent = 4)

    else:

        with open(secrets_file) as read_secrets_file:
            secrets = json.load(read_secrets_file)

        if secrets['api_key'] == '':
            if click.confirm('Your config file does not have an API key provided. Would you like to add one now?'):
                secrets['api_key'] = click.prompt('Enter your pagerduty API key')
            else:
                print('No API key provided. Ending execution')
                exit()

        # write the template to the file
        with open(secrets_file, 'w') as outfile:
            json.dump(secrets, outfile, indent = 4)