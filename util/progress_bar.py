# import libraries
import sys


# terminal progress bar to track
def progressBar(value, endvalue, bar_length=50):

    percent = float(value) / endvalue
    arrow = '-' * int(round(percent * bar_length)-1) + '>'
    spaces = ' ' * (bar_length - len(arrow))

    sys.stdout.write("\rProgress: [{0}] {1}% {2}/{3}".format(arrow + spaces, int(round(percent * 100)), value, endvalue))
    sys.stdout.flush()