# import libraries
import json

# import libraries from other files
from util.make_https_request import make_https_request


# get high priority incidents
def get_high_incidents(pd_key, dates_in_range, month, high_priority_file):

    print('Getting high priority incidents')
    print('\tThis may take a few minutes...')

    all_incidents = {
        'incidents': []
    }

    date_count = len(dates_in_range)
    counter = 0
    while counter < date_count:
        if counter == date_count - 1:
            break
        start_date = dates_in_range[counter]
        end_date = dates_in_range[counter + 1]

        # reset offset
        offset = 0
        # first call for the next date range and add the incidents to all_incidents
        paging_incidents = make_https_request(pd_key, start_date, end_date, 'high', offset)
        for inc in paging_incidents['incidents']:
            all_incidents['incidents'].append(inc)

        while paging_incidents['more']:
            offset = offset + 100
            if offset > 9900:
                break
            paging_incidents = make_https_request(pd_key, start_date, end_date, 'high', offset)
            for inc in paging_incidents['incidents']:
                all_incidents['incidents'].append(inc)

        counter = counter + 1

    with open(high_priority_file, 'w') as outfile:
        json.dump(all_incidents, outfile, indent=4)


# get low priority incidents
def get_low_incidents(pd_key, all_dates, month, low_priority_file):

    print('Getting low priority incidents')
    print('\tThis may take a few minutes...')

    all_incidents = {
        'incidents': []
    }

    date_count = len(all_dates)
    counter = 0
    while counter < date_count:
        if counter == date_count - 1:
            break
        start_date = all_dates[counter]
        end_date = all_dates[counter + 1]

        # reset offset
        offset = 0
        # first call for the next date range and add the incidents to all_incidents
        paging_incidents = make_https_request(pd_key, start_date, end_date, 'low', offset)
        for inc in paging_incidents['incidents']:
            all_incidents['incidents'].append(inc)

        while paging_incidents['more']:
            offset = offset + 100
            if offset > 9900:
                break
            paging_incidents = make_https_request(pd_key, start_date, end_date, 'low', offset)
            for inc in paging_incidents['incidents']:
                all_incidents['incidents'].append(inc)

        counter = counter + 1

    with open(low_priority_file, 'w') as outfile:
        json.dump(all_incidents, outfile, indent=4)