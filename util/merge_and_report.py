# import libraries
import json


# merge the high and low priority incident files
def merge_files(month, high_priority_file, low_priority_file, all_incidents_file):

    print('Merging incidents...')

    all_incidents = {'incidents': []}

    with open(high_priority_file) as paging_inc:
        all_paging = json.load(paging_inc)

        all_incidents['incidents'] = all_paging['incidents']

    with open(low_priority_file) as nonpaging_inc:
        all_nonpaging = json.load(nonpaging_inc)

    for inc in all_nonpaging['incidents']:
        all_incidents['incidents'].append(inc)

    with open(all_incidents_file, 'w') as outfile:
        json.dump(all_incidents, outfile, indent=4)