# import libraries
import pathlib
from datetime import timedelta, date


# convert the numerical date to a calendar month string
def read_config_parameters(start_date):

    print('Reading in config parameters')

    valid_months = {
        '01': 'january',
        '02': 'february',
        '03': 'march',
        '04': 'april',
        '05': 'may',
        '06': 'june',
        '07': 'july',
        '08': 'august',
        '09': 'september',
        '10': 'october',
        '11': 'november',
        '12': 'december'
    }

    # get the month value from start_date to compare to valid_months
    month_num = start_date
        
    if '-' in month_num:
        month_num = month_num.split('-')
        month_num = month_num[1]
    else:
        print('\tinvalid month value in --start-date date. please use format YYYY-MM-DD. terminating execution.')
        exit()

    # assign the value of the month number to the month name
    if month_num in valid_months:
        month = valid_months[month_num]
    else:
        print('\tinvalid month value in --start-date date. please use format YYYY-MM-DD. terminating execution.')
        exit()

    return month


# return a list of all the dates between start_date and end_date
def get_all_dates(start_date, end_date):

    all_dates = []

    for date in daterange(start_date, end_date):
        all_dates.append(date.strftime("%Y-%m-%d"))

    return all_dates


# get the delta between the dates
def daterange(start, end):
    for n in range(int ((end - start).days)+1):
        yield start + timedelta(n)