from setuptools import setup, find_packages


setup(
    name="pagerduty",
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click',
        'pathlib',
        'requests',
        'datetime'
    ],
    entry_points='''
        [console_scripts]
        pagerduty=run_pagerduty_report:cli
    ''',
)