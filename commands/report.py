# import libraries
import click
import json
from datetime import date

# import libraries from other files
from util.directory_management import directory_setup
from util.directory_management import cleanup_folders
from util.read_parameters import get_all_dates
from util.get_incidents import get_high_incidents
from util.get_incidents import get_low_incidents
from util.merge_and_report import merge_files
from util.get_incident_details import get_inc_details
from util.get_incident_details import update_all_incidents_details


# report command
@click.command(name = 'report', help = 'run a pagerduty report')
@click.option('--start-date', 'start_date', type = click.DateTime(formats = ['%Y-%m-%d']), required = True, help = 'the date to start the report from')
@click.option('--end-date', 'end_date', type = click.DateTime(formats = ['%Y-%m-%d']), default = str(date.today()), required = False, help = 'defaults to today')
def report(start_date, end_date):

    # setup required folders and paths
    # return the month of the report and the paths to the required files
    month, cache_folder, secrets_file, high_priority_file, low_priority_file, all_incidents_file, incident_details_file, final_report_file = directory_setup(start_date, end_date)

    # get the date range so we can process day by day
    dates_in_range = get_all_dates(start_date, end_date)

    # read the pagerduty api key from the secrets file
    with open(secrets_file) as key_file:
        pd_key = json.load(key_file)

    pd_key = pd_key['api_key']

    # get the high priority incidents
    get_high_incidents(pd_key, dates_in_range, month, high_priority_file)

    # get the low priority incidents
    get_low_incidents(pd_key, dates_in_range, month, low_priority_file)

    # merge the high and low incidents
    merge_files(month, high_priority_file, low_priority_file, all_incidents_file)

    # get more details for all incidents
    get_inc_details(pd_key, month, all_incidents_file, incident_details_file)

    # update the report with the details
    update_all_incidents_details(month, incident_details_file, final_report_file)

    print('Writing file to reports directory')

    print('Cleaning up cache files')
    cleanup_folders(cache_folder)

    print(f'\toutput file: {final_report_file}')
    print('Complete!')