# import libraries
import click

# import libraries from other files
import commands.report as cmd_report


# override the help command defaults
CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


# start cli here
@click.group(context_settings = CONTEXT_SETTINGS)
@click.pass_context
def cli(ctx):

    pass


# pagerduty cli commands
cli.add_command(cmd_report.report)