# PagerDuty Incident Reporting

This small Python script is used by PagerDuty Administrators to execute a report of all incidents that are currently assigned and have been previously acknowledged and resolved by the NOC. This report will return a consolidated JSON file sorted by each incident title and contain a list of the hosts that triggered for each incident.

## Requirements

In order to execute this script Python 3.7 or higher must be installed on the machine as well as pipenv.  
The latest version of Python can be downloaded from here: [https://www.python.org/downloads/](https://www.python.org/downloads/)  
Once Python is installed you can install pipenv.  
MacOS: pipenv can be installed via brew -- **brew install pipenv**  
Windows: pipenv can be install via PowerShell -- **pip install pipenv**  

## Creating the Hidden Secrets Folder and API Key File

Once the Python requirements are met and you have cloned the repository to your machine you will need to create a hidden folder called secrets and inside that folder add a hidden file called pd_api_key.json.
This hidden folder with the hidden file will reside within the second pagerduty-inc-reporting folder of the repository along with the reports and config folder.  
MacOS -- **mkdir .secrets**  
Windows -- **New-Item -Name pagerduty-inc-reporting\\.secrets -ItemType directory; attrib +h .secrets**  

Once the hidden folder exists you can create your pd_api_key.json file.  
MacOS -- **touch .secrets/.pd_api_key.json**  
Windows -- **New-Item -Name pagerduty-inc-reporting\\.secrets\\.pd_api_key.json -ItemType file; attrib +h .pd_api_key.json**  

Once you have the pd_api_jey.json file open it in your text editor and paste the following format:  

```json
{
    "api_key":  "<enter api key here>"
}
```

> A PagerDuty Administrator can provide you the existing PagerDuty ReadOnly API key to enter as a String in the above JSON for the value of the api_key key

## Create your Python Virtual Environment

Why Python virtual environments?

A Pipfile has been included in this repository so that all of the Python dependencies are included and the end user does not have to worry about having the correct package dependencies installed.

Create a Python virtual environment:  
With pipenv installed you can now install the dependencies from the Pipfile.  
From within the main pagerduty-inc-reporting folder containing the Pipfile execute -- **pipenv install**  
This will take a moment and output as it installs the additional package requirements to run the script.  

Once the install command completes you now have a virtual environment that you can then execute the script inside of!  

## Execute the CLI Command

To execute the **pagerduty** CLI command have pip install the setup file -- **pip install --editable .**
> Do no forget to include the period after --editable. This allows the setup file to configure the command to work within the repository.

## Setting up the config.json

The config.json file controls the dates used to lookup the incidents from.  
It contains two keys. The first *begin*, to specify the date to start pulling from and *end*, to specify when to stop looking.  
This file resides in the config folder under the second pagerduty-inc-reporting folder and can be edited in any text editor you already have installed.  
> The execution of a query is scoped to a 31 day window to accommodate running a full monthly report.  
> example: begin: 2020-01-01, end: 2020-02-01 will get all incidents beginning the first second of January first all the way up until the last second of the last day of January.  

Set these values appropriately for the date range you want to look up from and save the file.

## Executing the Script in the Virtual Environment

With the package dependencies installed and the setup file installed the command can now be executed.  

Run **pagerduty report -h** to see the help file. This will list the parameters that are required to run the command.  

## Examples

Get the help files -- **pagerduty report -h**  
Run a report with all options specified -- **pagerduty report --start-date 2020-06-30 --end-date 2020-07-1**  
The **--end-date** option is not required to provide since it defaults to the current date  
Executing **pagerduty report --start-date 2020-06-30** will provide the same result as the command above  


> IMPORTANT NOTE: Depending on the length of the date range provided this script can take up to a few hours to run while it collects the incident details from PagerDuty API  

The script will attempt to gather both high and low priority incidents separately per day indicated within the date range specified in the config.json and then merge them. After merging them it will collect additional details for each incident. During this phase your terminal will display a progress bar for you to monitor. Once all the incident details are collected it will run a few calculations to enhance the data and then it will create a JSON report in the reports folder and lastly it will output the file location to your terminal.
